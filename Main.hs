{-
- Student name  :   Horatiu Bota
- Student matric:   0900573b
- Course        :   Functional Programming 4
- CW Title      :   Programming Language Interpreter
- Date          :   04/11/2012 01:08 
- 
- Status report:
- This file is an implementation of the parser and the interpreter required for Assessed Exercise 1. It also contains 6/10 suggested extensions.
- In my judgement, the parser and interpreter function correctly. 
-
- Implemented extensions:
- (*) More numeric operators have been added - power operator
- (*) More boolean expressions have been added (==) and there is allowance for AND, OR, NOT operators
- (*) Double data type is available
- (*) Concrete syntax has been significantly extended
- (*) Operator precendence is handled
- (*) Error messages at parsing level have been improved
-
- For testing purposes, I have used the Hspec library. In order to run the tests, you must have Hspec installed on your system. 
- This can be performed by running "cabal install hspec" in a terminal window. 
-
- The tests have been commented out as well as the import statement for Hspec.
-
- The structure of this program is as follows:
- (1) Abstract syntax definitions
- (2) Lexer and Parser (using Parsec)
- (3) Interpreter
- (4) Test suit
-
- Usage: Main [-h] [filename...]
-
-}

import System.IO
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

import System.Environment
import System.Exit

import Data.Map
import Data.Maybe
import Data.Either

--import Test.Hspec

type Var = String
type Decl = Var

data Number = NumInt Integer
            | NumDbl Double
            deriving (Read, Show, Eq, Ord)

class Operations a where
  (.+.) :: a -> a -> a
  (.-.) :: a -> a -> a
  (.*.) :: a -> a -> a
  (./.) :: a -> a -> a
  (.^.) :: a -> a -> a

  (.=.) :: a -> a -> Bool
  (.<.) :: a -> a -> Bool
  (.>.) :: a -> a -> Bool
  
  neg :: a -> a

instance Operations Number where
  (.+.) (NumInt a) (NumInt b) = NumInt (a + b)
  (.+.) (NumDbl a) (NumDbl b) = NumDbl (a + b)
  (.+.) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) + b)
  (.+.) (NumDbl a) (NumInt b) = NumDbl (a + (fromInteger b))

  (.-.) (NumInt a) (NumInt b) = NumInt (a - b)
  (.-.) (NumDbl a) (NumDbl b) = NumDbl (a - b)
  (.-.) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) - b)
  (.-.) (NumDbl a) (NumInt b) = NumDbl (a - (fromInteger b))

  (.*.) (NumInt a) (NumInt b) = NumInt (a * b)
  (.*.) (NumDbl a) (NumDbl b) = NumDbl (a * b)
  (.*.) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) * b) 
  (.*.) (NumDbl a) (NumInt b) = NumDbl (a * (fromInteger b))

  (./.) (NumInt a) (NumInt b) = NumInt (a `div` b)
  (./.) (NumDbl a) (NumDbl b) = NumDbl (a / b)
  (./.) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) / b) 
  (./.) (NumDbl a) (NumInt b) = NumDbl (a / (fromInteger b))

  (.^.) (NumInt a) (NumInt b) = NumInt (a ^ b)
  (.^.) (NumDbl a) (NumDbl b) = NumDbl (a ** b)
  (.^.) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) ** b) 
  (.^.) (NumDbl a) (NumInt b) = NumDbl (a ** (fromInteger b))

  (.=.) (NumInt a) (NumInt b) = a == b
  (.=.) (NumDbl a) (NumDbl b) = a == b
  (.=.) (NumInt a) (NumDbl b) = (fromInteger a) == b 
  (.=.) (NumDbl a) (NumInt b) = a == (fromInteger b)

  (.>.) (NumInt a) (NumInt b) = a > b
  (.>.) (NumDbl a) (NumDbl b) = a > b
  (.>.) (NumInt a) (NumDbl b) = (fromInteger a) > b 
  (.>.) (NumDbl a) (NumInt b) = a > (fromInteger b)

  (.<.) (NumInt a) (NumInt b) = a < b
  (.<.) (NumDbl a) (NumDbl b) = a < b
  (.<.) (NumInt a) (NumDbl b) = (fromInteger a) < b 
  (.<.) (NumDbl a) (NumInt b) = a < (fromInteger b)

  neg (NumInt a) = NumInt ((-1) * a)
  neg (NumDbl a) = NumDbl ((-1) * a)


data NumExp = NVar          Var                 -- integer variables
            | NCon          Number
            | Neg           NumExp              -- negate integer expression
            | NumOperation  NumOp NumExp NumExp -- integer operations
            deriving (Show)

data NumOp = Add
           | Subtract
           | Multiply
           | Divide
           | Power
           deriving (Show)

data BoolExp = BCon Bool                        -- bool constants
             | Not BoolExp                      -- bool negation
             | BOperation BOp BoolExp BoolExp   -- bool binary operations (and, or)
             | ROperation ROp NumExp  NumExp    -- bool relational operations (less, greater, equals)
             deriving (Show)

data BOp = And 
         | Or 
         deriving (Show)
         
data ROp = Greater 
         | Less
         | Equals
         deriving (Show)

data Stmt = Begin [Decl] [Stmt]
          | Assign Var NumExp
          | Seq [Stmt]
          | Read Var
          | Write NumExp
          | IfThenElse BoolExp Stmt Stmt
          | While BoolExp Stmt
          deriving (Show)

--
--
-- Parsing
--
--


languageDef = 
    emptyDef { Token.commentStart    = "/*"
             , Token.commentEnd      = "*/"
             , Token.commentLine     = "//"
             , Token.identStart      = letter
             , Token.identLetter     = alphaNum
             , Token.reservedNames   = ["if", "then", "else", "while",
                                     "do", "skip", "true", "false",
                                     "not", "and", "or", "begin", "end",
                                     "read", "write"
                                       ]
             , Token.reservedOpNames = ["+", "-", "*", "/", ":=", "^",
                                        "<", ">", "==", "and", "or", "not"]
             }

lexer = Token.makeTokenParser languageDef -- Binding lexical parsers

identifier = Token.identifier lexer
reserved   = Token.reserved   lexer
reservedOp = Token.reservedOp lexer
parens     = Token.parens     lexer
integer    = Token.integer    lexer
float      = Token.float      lexer
semi       = Token.semi       lexer
whiteSpace = Token.whiteSpace lexer
brackets   = Token.brackets   lexer -- []
commaSep   = Token.commaSep   lexer
braces     = Token.braces     lexer -- {}

intOperators = [ [Prefix (reservedOp "-"   >>   return (Neg                  ))          ]
               , [Infix  (reservedOp "*"   >>   return (NumOperation Multiply)) AssocLeft] 
               , [Infix  (reservedOp "/"   >>   return (NumOperation Divide  )) AssocLeft]
               , [Infix  (reservedOp "+"   >>   return (NumOperation Add     )) AssocLeft]
               , [Infix  (reservedOp "-"   >>   return (NumOperation Subtract)) AssocLeft]
               , [Infix  (reservedOp "^"   >>   return (NumOperation Power   )) AssocLeft]
               ]

boolOperators = [ [Prefix (reserved "not"   >>   return (Not                ))          ]
                , [Infix  (reserved "and"   >>   return (BOperation And     )) AssocLeft]
                , [Infix  (reserved "or"    >>   return (BOperation Or      )) AssocLeft]
                ]

numExpression :: Parser NumExp
numExpression = buildExpressionParser intOperators intTerm
            <?> "numeric expression"

intTerm :: Parser NumExp
intTerm = parens numExpression
      <|> liftM NVar identifier
      <|> do
            var <- try float 
            return (NCon (NumDbl var))
      <|> do
            var <- integer
            return (NCon (NumInt var))
      <?> "Parse error: error parsing numeric type"

numConst :: Parser Number
numConst = do
            var <- try float
            return (NumDbl var)
       <|> do
            var <- try integer
            return (NumInt var)
       <?> "Parse error: error parsing numeric constant"


boolExpression :: Parser BoolExp
boolExpression = buildExpressionParser boolOperators boolTerm
             <?> "boolean expression"

boolTerm :: Parser BoolExp
boolTerm = parens boolExpression
       <|> (reserved "true" >> return (BCon True))
       <|> (reserved "false">> return (BCon False))
       <|> relExpression
       <?> "Parse error: invalid boolean term"

relExpression = 
    do numExp1 <- numExpression
       op      <- relation 
       numExp2 <- numExpression
       return $ ROperation op numExp1 numExp2 

relation = (reservedOp ">" >> return Greater)
       <|> (reservedOp "<" >> return Less)
       <|> (reservedOp "==">> return Equals)
       <?> "Parse error: invalid boolean relation"


mainParser :: Parser Stmt             -- Statement parsers start here
mainParser = whiteSpace >> statement

statement :: Parser Stmt
statement = parens statement
        <|> sequenceOfStmt

sequenceOfStmt =
    do list <- (sepBy1 statement' semi)
       return $ if length list == 1 then head list else Seq list

sequenceOfStmt' = 
    do list <- many1 statement'
       return list

statement' :: Parser Stmt
statement' = try (beginSeq)
         <|> beginStmt
         <|> assignStmt
         <|> readStmt
         <|> writeStmt
         <|> ifStmt
         <|> whileStmt
         <?> "Parse error: invalid statement"

beginStmt :: Parser Stmt
beginStmt =
    do reserved "begin"
       d <- decl
       s <- sequenceOfStmt'
       reserved "end" 
       return $ Begin d s

beginSeq :: Parser Stmt
beginSeq = 
    do reserved "begin"
       s <- sequenceOfStmt'
       reserved "end"
       return $ Seq s

decl :: Parser [Decl]
decl =
  do list <- brackets (commaSep identifier)
     return list

--
variable :: Parser NumExp
variable = 
    do var <- identifier
       return $ NVar var
--

assignStmt :: Parser Stmt
assignStmt = 
    do var <- identifier
       reservedOp ":="
       expr <- numExpression
       semi
       return $ Assign var expr

readStmt :: Parser Stmt
readStmt = 
    do reserved "read"
       var <- identifier
       semi
       return $ Read var

writeStmt :: Parser Stmt
writeStmt =
    do reserved "write"
       exp <- numExpression
       semi
       return $ Write exp

ifStmt :: Parser Stmt
ifStmt = 
    do reserved "if"
       b  <- boolExpression
       reserved "then"
       e1 <- statement
       reserved "else"
       e2 <- statement
       return $ IfThenElse b e1 e2

whileStmt :: Parser Stmt
whileStmt = 
    do reserved "while"
       b <- boolExpression
       reserved "do"
       e <- statement
       return $ While b e

parseTestHelper prsr str = case parse prsr "" str of
                                Left e -> error $ show e
                                Right r -> r

parseString :: String -> Stmt
parseString str = case parse mainParser "" str of
                    Left e  -> error $ show e
                    Right r -> r

parseFile file = 
  do program <- readFile file
     case parse mainParser "" program of
       Left e -> print e >> fail "parse error"
       Right r -> print r

parseNumber :: String -> Number
parseNumber str = case parse numConst "" str of
                    Left e -> error $ show e
                    Right r -> r

--
--
-- Interpreter
--
--

type Env = [[(Var, Number)]]

--lookup and update functions

nlookup :: Var -> Env -> Maybe Number
nlookup var []       = Nothing
nlookup var (x:xs)   = if isJust $ Prelude.lookup var x then Prelude.lookup var x
                       else nlookup var xs

nupdate :: Var -> Number -> Env -> Env
nupdate var newVal []     = []
nupdate var newVal (x:xs) = if isJust $ Prelude.lookup var x then (toList $ alter f var (fromList x)) : xs
                            else x : (nupdate var newVal xs)
                             where 
                                 f _ = Just newVal


-- evaluating numeric expressions
evalNumExp :: NumExp -> Env -> Number
evalNumExp (NVar x) e = if nlookup x e /= Nothing then fromJust (nlookup x e)
                        else error $ "EVAL INT EXP: no variable declaration: " ++ x

evalNumExp (NCon c) e = c
evalNumExp (Neg  n) e = neg (evalNumExp n e)
evalNumExp (NumOperation Add      numExp1 numExp2) e = evalNumExp numExp1 e   .+.   evalNumExp numExp2 e
evalNumExp (NumOperation Subtract numExp1 numExp2) e = evalNumExp numExp1 e   .-.   evalNumExp numExp2 e
evalNumExp (NumOperation Multiply numExp1 numExp2) e = evalNumExp numExp1 e   .*.   evalNumExp numExp2 e
evalNumExp (NumOperation Divide   numExp1 numExp2) e = evalNumExp numExp1 e   ./.   evalNumExp numExp2 e
evalNumExp (NumOperation Power    numExp1 numExp2) e = evalNumExp numExp1 e   .^.   evalNumExp numExp2 e

evalBoolExp :: BoolExp -> Env -> Bool
evalBoolExp (BCon b) _ = b
evalBoolExp (Not  b) e = not $ evalBoolExp b e 
evalBoolExp (BOperation And exp1 exp2) e = evalBoolExp exp1 e && evalBoolExp exp2 e
evalBoolExp (BOperation Or  exp1 exp2) e = evalBoolExp exp1 e || evalBoolExp exp2 e
evalBoolExp (ROperation Greater numExp1 numExp2) e = evalNumExp numExp1 e .>. evalNumExp numExp2 e
evalBoolExp (ROperation Less    numExp1 numExp2) e = evalNumExp numExp1 e .<. evalNumExp numExp2 e
evalBoolExp (ROperation Equals  numExp1 numExp2) e = evalNumExp numExp1 e .=. evalNumExp numExp2 e 

readValue :: IO Number
readValue = do
             val <- getLine  
             return (parseNumber val)
 
printVariable :: Var -> Env -> IO ()
printVariable var env = if nlookup var env == Nothing then error $ "PRINT VAR: no variable declaration: " ++ var -- exception caught from nlookup
                        else printAux (fromJust (nlookup var env))

-- helper function for printing numeric types
printAux :: Number -> IO ()
printAux (NumInt a) = do print a
printAux (NumDbl a) = do print a

interpretRead :: Var -> Env -> IO Env
interpretRead  var e = if nlookup var e == Nothing then error "var"
                       else do val <- readValue
                               return (nupdate var val e)

interpretWrite :: NumExp -> Env -> IO Env
interpretWrite nExp e = do printAux (evalNumExp nExp e)
                           return e

                          
interpretAssign :: Var -> NumExp -> Env -> IO Env
interpretAssign var numExp env = return (nupdate var (evalNumExp numExp env) env)

switchIf :: BoolExp -> Stmt -> Stmt -> Env -> Stmt
switchIf bExp stmt1 stmt2 env = if evalBoolExp bExp env then stmt1
                                else stmt2                                  
                                
-- main interpret function                                
interpret :: Stmt -> Env -> IO Env
interpret (Read var) env                    = interpretRead var env
interpret (Write nExp) env                  = interpretWrite nExp env
interpret (Assign var numExp) env           = interpretAssign var numExp env
interpret (IfThenElse bExp stmt1 stmt2) env = interpret (switchIf bExp stmt1 stmt2 env) env
interpret (While bExp stmt) env             = do
                                                if evalBoolExp bExp env 
                                                    then do 
                                                            newEnv <- interpret stmt env
                                                            interpret (While bExp stmt) newEnv
                                                else
                                                     return env
interpret (Seq []) env                      = return env
interpret (Seq stmts) env                   = do
                                                newEnv <- interpret (Seq (init stmts)) env
                                                interpret (last stmts) newEnv
interpret (Begin [] []) env                 = return env
interpret (Begin [] stmts) env              = interpret (Seq stmts) env
interpret (Begin decls []) env              = return ((initScope decls) : env)
interpret (Begin decls stmts) env           = do
                                                newEnv <- interpret (Seq stmts) ((initScope decls) : env)
                                                return (popScope newEnv)

-- initializing scope
initScope :: [Var] -> [(Var, Number)]
initScope decls = zip decls zeros
                  where
                      zeros = (NumInt 0) : zeros

-- popping most local scope from Env
popScope :: Env -> Env
popScope [] = []
popScope env = tail env

--
--
-- Main
--
--


main = do
  args1 <- getArgs
  argparse (head args1)
  run (head args1)

run file = 
  do program <- readFile file
     case parse mainParser "" program of
       Left e -> print e >> fail "parse error"
       Right r -> interpret r []

argparse "-h" = usage >> exit
argparse str  = return ()

usage = putStrLn "Usage: Main [-h] [filename...]"
exit  = exitWith ExitSuccess

--
--
-- Testing
--
--

--
-- Interpreter testing
--
{-
scope_   = [("a", NumInt 1), ("b", NumInt 2)] :: [(Var, Number)]
scope_'  = [("x", NumDbl 2.3), ("y", NumDbl 3.4), ("z", NumDbl 4.5)] :: [(Var, Number)]
scope_'' = [("a", NumDbl 101), ("x", NumInt 102), ("w", NumInt 103)] :: [(Var, Number)]

testEnv = [scope_, scope_', scope_'']

run_interpreter_test = hspec $ do
  describe "interpreter function --- nlookup" $ do

    it "returns the first occurrence of the variable \"a\", from most localized scope" $
      nlookup "a" testEnv `shouldBe` Just (NumInt 1)
    it "returns the first occurrence of variable \"a\", from global scope" $
      nlookup "a" [scope_', scope_''] `shouldBe` Just (NumDbl 101)
    it "returns Nothing when looking for a variable that isn't declared -- \"var\"" $
      nlookup "var" testEnv `shouldBe` Nothing
    it "returns Nothing from looking up variables in an empty environment" $
      nlookup "a" [] `shouldBe` Nothing

  describe "interpreter function --- nupdate" $ do

    it "updates the value of a variable in local scope" $
      nupdate "a" (NumInt 5) [scope_] `shouldBe` [ ("a", NumInt 5) : tail scope_ ]
    it "updates the value of a variable in global scope" $
      nupdate "a" (NumInt 0) [scope_', scope_''] `shouldBe` [scope_', [("a", NumInt 0), ("w", NumInt 103), ("x", NumInt 102)]]
    it "does NO update to an empty list" $
      nupdate "a" (NumInt 10) [] `shouldBe` []
    it "does NOT modify a list that does not contain the variable we are looking for" $
      nupdate "a" (NumInt 102) [scope_'] `shouldBe` [scope_']

  describe "interpreter function --- evalNumExp" $ do
    
    it "evaluates numeric constants" $
      evalNumExp (NCon (NumInt 1)) testEnv `shouldBe` NumInt 1
    it "evaluates variables" $
      evalNumExp (NVar "a") testEnv `shouldBe` NumInt 1

    -- Addition operations  
    it "evaluates integer addition operator" $
      evalNumExp (NumOperation Add (NVar "a") (NCon (NumInt 1))) testEnv `shouldBe` NumInt 2
    it "evaluates double addition operator" $
      evalNumExp (NumOperation Add (NVar "x") (NCon (NumDbl 1.3))) testEnv `shouldBe` NumDbl 3.6
    it "evaluates mixed addition operator" $
      evalNumExp (NumOperation Add (NVar "a") (NCon (NumDbl 1.3))) testEnv `shouldBe` NumDbl 2.3
    
    -- Subtraction operations
    it "evaluates integer subtraction operator" $
      evalNumExp (NumOperation Subtract (NVar "a") (NCon (NumInt 9))) testEnv `shouldBe` NumInt (-8)
    it "evaluates double addition operator" $
      evalNumExp (NumOperation Subtract (NVar "x") (NCon (NumDbl 1.3))) testEnv `shouldBe` NumDbl (3.6)
    it "evaluates mixed addition operator" $
      evalNumExp (NumOperation Subtract (NVar "a") (NCon (NumDbl 1.3))) testEnv `shouldBe` NumDbl (-0.3)
 
    -- Multiplication operations
    it "evaluates integer multiplication operator" $
      evalNumExp (NumOperation Multiply (NVar "a") (NCon (NumInt 10))) testEnv `shouldBe` NumInt 10
    it "evaluates double multiplication operator" $
      evalNumExp (NumOperation Multiply (NVar "x") (NCon (NumDbl 1.3))) testEnv `shouldBe` NumDbl 2.9899999999999998
    it "evaluates mixed multiplication operator" $
      evalNumExp (NumOperation Multiply (NVar "b") (NCon (NumDbl 1.3))) testEnv `shouldBe` NumDbl 2.6
 
    -- Division operations  
    it "evaluates integer division operator" $
      evalNumExp (NumOperation Divide (NVar "x") (NCon (NumInt 2))) [scope_''] `shouldBe` NumInt 51
    it "evaluates double division operator" $
      evalNumExp (NumOperation Divide (NVar "x") (NCon (NumDbl 1.3))) testEnv `shouldBe` NumDbl 1.769230769230769
    it "evaluates mixed division operator" $
      evalNumExp (NumOperation Divide (NVar "x") (NCon (NumDbl 1.3))) [scope_''] `shouldBe` NumDbl 78.46153846153845
 
     -- Power operations  
    it "evaluates integer power operator" $
      evalNumExp (NumOperation Power (NVar "b") (NCon (NumInt 2))) testEnv `shouldBe` NumInt 4
    it "evaluates double power operator" $
      evalNumExp (NumOperation Power (NVar "x") (NCon (NumDbl 1.3))) testEnv `shouldBe` NumDbl 2.952882641412121 
    it "evaluates mixed power operator" $
      evalNumExp (NumOperation Power (NVar "x") (NCon (NumDbl 1.3))) [scope_''] `shouldBe` NumDbl 408.488865654784 
  
    -- Negation
    it "evaluates negation of a NumExp" $
      evalNumExp (Neg (NCon (NumInt 1))) testEnv `shouldBe` NumInt (-1)

  describe "interpreter functions --- evalBoolExp" $ do    

    it "evaluates boolean constants" $
        evalBoolExp (BCon True) testEnv `shouldBe` True
    it "evaluates boolean unary operator NOT" $
        evalBoolExp (Not (BCon True)) testEnv `shouldBe` False
    it "evaluates boolean binary operator AND" $
        evalBoolExp (BOperation And (BCon True) (BCon False)) testEnv `shouldBe` False
    it "evaluates boolean binary operator OR" $
        evalBoolExp (BOperation Or (BCon True) (BCon False)) testEnv `shouldBe` True
    it "evaluates boolean relational operator LESS" $
        evalBoolExp (ROperation Less (NCon (NumInt 1)) (NCon (NumInt 2))) testEnv `shouldBe` True
    it "evaluates boolean relational operator GREATER" $
        evalBoolExp (ROperation Greater (NCon (NumInt 1)) (NCon (NumInt 2))) testEnv `shouldBe` False
    it "evaluates boolean relational operation EQUALS" $
        evalBoolExp (ROperation Equals (NCon (NumInt 1)) (NCon (NumInt 2))) testEnv `shouldBe` False
-}
--
--Parser testing
--
  
{-
 - beginStmt
 - parseTestHelper beginStmt "begin [a,b,c] a:=1; end" 
 - parseTestHelper beginStmt "begin [a,b,c] end"       --- fails
 -
 - beginSeq
 - parseTestHelper beginSeq "begin a:=1; end"
 - parseTestHelper beginSeq "begin end"                --- fails
 -
 - ifStmt
 - parseTestHelper ifStmt "if a>b then a:=b; else b:=a;"
 - parseTestHelper ifStmt "if a>b then a:=b; b:=a; a:=b; else b:=a;" --- fails
 - parseTestHelper ifStmt "if a>b then begin a:=b; b:=a; a:=b; end else b:=a;" --- showing that sequence of operations need to be wrapped
 -
 - whileStmt
 - parseTestHelper whileStmt "while a>b do begin a:=b; b:=a; a:=b; end"
 - parseTestHelper whileStmt "while a>b do a:=a+1;"
 -
 - readStmt
 - parseTestHelper readStmt "read a;"
 - parseTestHelper readStmt "read 1;" --- fails
 -
 - writeStmt
 - parseTestHelper writeStmt "write a;"
 - parseTestHelper writeStmt "write 1;" --- fails
 -
 - assignStmt
 - parseTestHelper assignStmt "a:=1;"
 - parseTestHelper assignStmt "a:=a+1";
 -
 - NumExp parsing
 - parseTestHelper numExpression "1+2"
 - parseTestHelper numExpression "(a^2 - b^2) * (a^2 + b^2)^2 / 4 - 100"
 - parseTestHelper numExpression "a + 2 * 3 + ( a / 2 - 3)" --- showing order of operations
 -
 - BoolExp parsing
 - parseTestHelper boolExpression "true"
 - parseTestHelper boolExpression "a == b"
 - parseTestHelper boolExpression "a < b and b > a and a < b or b == a"
 - parseTestHelper boolExpression "a < b and b > a and a < b or true and false"
 - parseTestHelper boolExpression "a < b and b > a and a < b or true and 1 + 2 > 1"
 -
-}

-- 
-- Main test -- programs to be executed using interpreter
-- Usage: Main [-h] [filename...]
--

{--

begin [i, n]
   i := 5.0;
   while i > 0 do
      begin [n]
        i := i - 1;
        write i;
        n := (n+2) ^ 3.4;
        write n;
        end
   write i;
end

--}

{-
begin [a,b,c,d]
	write 2+1*6; // shows precedence working correctly
	a:=888.8;
	b:=50;
	if (b>a) then
		write b; // currently does not have semi colon as single statement
	else
		begin [z]
			write a;
			read z;
			write z*2;
			begin [a]
				read a;
			end
		end
	write a;
	read d;
	while (d<b and true) do
		begin
			write b;
			write d;
			d := ((d*2)-1) ^ 2.5;
		end
end
-}
