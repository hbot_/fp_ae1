import System.IO
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

import Data.Map
import Data.Maybe

type Var = String
type Decl = Var
type Env = [[(Var, Integer)]]

data IntExp = IVar          Var                 -- integer variables
            | ICon          Integer             -- integer constants
            | Neg           IntExp              -- negate integer expression
            | IntOperation  IntOp IntExp IntExp -- integer operations
            deriving (Show)

data IntOp = Add
           | Subtract
           | Multiply
           | Divide
           deriving (Show)

data BoolExp = BCon Bool                        -- bool constants
             | Not BoolExp                      -- bool negation
             | BOperation BOp BoolExp BoolExp   -- bool binary operations (and, or)
             | ROperation ROp IntExp  IntExp   -- bool relational operations (less, greater, equals)
             deriving (Show)

data BOp = And 
         | Or 
         deriving (Show)
         
data ROp = Greater 
         | Less
         | Equals
         deriving (Show)

data Stmt = Begin [Decl] [Stmt]
          | Assign Var IntExp
          | Seq [Stmt]
          | Read Var
          | Write IntExp
          | IfThenElse BoolExp Stmt Stmt
          | While BoolExp Stmt
          deriving (Show)
          
          
mlookup :: Var -> Env -> Maybe Integer
mlookup var []  = error $ "LOOKUP: no variable declaration: " ++ var
mlookup var env = Prelude.lookup var (head env)

minsert :: Var -> Integer -> Env -> Env
minsert var val []  = (toList $ insert var val (fromList [])) : []
minsert var val env = (toList $ insert var val (fromList (head env))) : []

mupdate :: Var -> Integer -> Env -> Env
mupdate var newVal []  = (toList $ alter f var (fromList [])) : []
                          where
                              f _ = Just newVal
mupdate var newVal env = (toList $ alter f var (fromList (head env))) : []
                          where
                              f _ = Just newVal

evalIntExp :: IntExp -> Env -> Integer
evalIntExp (IVar x) e = if mlookup x e /= Nothing then fromJust (mlookup x e)
                        else error $ "EVAL INT EXP: no variable declaration: " ++ x
evalIntExp (ICon c) e = c
evalIntExp (Neg  n) e = (-1) * evalIntExp n e
evalIntExp (IntOperation Add      intExp1 intExp2) e = evalIntExp intExp1 e   +   evalIntExp intExp2 e
evalIntExp (IntOperation Subtract intExp1 intExp2) e = evalIntExp intExp1 e   -   evalIntExp intExp2 e
evalIntExp (IntOperation Multiply intExp1 intExp2) e = evalIntExp intExp1 e   *   evalIntExp intExp2 e
evalIntExp (IntOperation Divide   intExp1 intExp2) e = evalIntExp intExp1 e `div` evalIntExp intExp2 e

evalBoolExp :: BoolExp -> Env -> Bool
evalBoolExp (BCon b) _ = b
evalBoolExp (Not  b) e = not $ evalBoolExp b e 
evalBoolExp (BOperation And exp1 exp2) e = evalBoolExp exp1 e && evalBoolExp exp2 e
evalBoolExp (BOperation Or  exp1 exp2) e = evalBoolExp exp1 e || evalBoolExp exp2 e
evalBoolExp (ROperation Greater intExp1 intExp2) e = evalIntExp intExp1 e  > evalIntExp intExp2 e
evalBoolExp (ROperation Less    intExp1 intExp2) e = evalIntExp intExp1 e  < evalIntExp intExp2 e
evalBoolExp (ROperation Equals  intExp1 intExp2) e = evalIntExp intExp1 e == evalIntExp intExp2 e 

readValue :: IO Integer
readValue = do
             tmp <- getLine
             return (read tmp)
             
printVariable :: Var -> Env -> IO ()
printVariable var env = if mlookup var env == Nothing then error $ "PRINT VAR: no variable declaration: " ++ var -- exception caught from mlookup
                        else print (fromJust (mlookup var env)) 

interpretRead :: Var -> Env -> IO Env
interpretRead  var e = if mlookup var e == Nothing then error "var"
                       else do val <- readValue
                               return (mupdate var val e)

interpretWrite :: Var -> Env -> IO Env
interpretWrite var e = do printVariable var e
                          return e
                          
interpretAssign :: Var -> IntExp -> Env -> Env
interpretAssign var intExp env = mupdate var (evalIntExp intExp env) env

switchIf :: BoolExp -> Stmt -> Stmt -> Env -> Stmt
switchIf bExp stmt1 stmt2 env = if evalBoolExp bExp env then stmt1
                                else stmt2
                                
interpret :: Stmt -> Env -> IO Env
interpret (Read var) env                    = interpretRead var env
interpret (Write (IVar var)) env            = interpretWrite var env
interpret (Assign var intExp) env           = return (interpretAssign var intExp env)
interpret (IfThenElse bExp stmt1 stmt2) env = interpret (switchIf bExp stmt1 stmt2 env) env
interpret (While bExp stmt) env             = do
                                                if evalBoolExp bExp env 
                                                    then do 
                                                            newEnv <- interpret stmt env
                                                            interpret (While bExp stmt) newEnv
                                                else return env
interpret (Seq []) env                      = return env
interpret (Seq stmts) env                   = do
                                                newEnv <- interpret (Seq (init stmts)) env
                                                interpret (last stmts) newEnv
interpret (Begin [] []) env                 = return env
interpret (Begin decls []) env              = return env
interpret (Begin decls stmts) env           = interpret (Seq stmts) (initEnv decls)

initEnv :: [Var] -> Env
initEnv []    = []
initEnv decls = minsert (head decls) 0 (initEnv (tail decls))

liftEnv :: Env -> IO Env
liftEnv env = return (env)

liftInt :: Integer -> IO Integer
liftInt i = return i
