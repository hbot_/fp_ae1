import System.IO
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

data IntExp
  = IVar String
  | ICon Integer
  | Neg IntExp
  | IntBinary IntBinaryOp IntExp IntExp
  deriving (Show)

data BoolExp
  = BoolConst Bool
  | Not BoolExp
  | BoolBinary BoolBinaryOp BoolExp BoolExp -- for 'AND', 'OR'
  | RelBinary  RelBinaryOp  IntExp  IntExp  -- for '<', '>', '='
  deriving (Show)

--
-- Binary operations on booleans
--

-- Boolean operations 'AND', 'OR'

data BoolBinaryOp
  = And
  | Or 
  deriving (Show)

-- Relational boolean operations '<', '>', '='

data RelBinaryOp
  = Greater
  | Less
  | Equals
  deriving (Show)

--
-- Binary operations on integers
--

data IntBinaryOp
  = Add
  | Subtract
  | Multiply
  | Divide
  deriving (Show)

--
-- Statements
--

type Decl = String

data Stmt
  = Begin [Decl] [Stmt]
  | Seq [Stmt]
  | Assign String IntExp
  | Read String
  | Write IntExp
  | IfThenElse BoolExp Stmt Stmt
  | While BoolExp Stmt
  | Skip
  deriving (Show)

--
-- Language definition
--

languageDef = 
  emptyDef { Token.commentStart   = "/*"
           , Token.commentEnd     = "*/"
           , Token.commentLine    = "//"
           , Token.identStart     = letter
           , Token.identLetter     = alphaNum
           , Token.reservedNames   = ["if", "then", "else", "while",
                                     "do", "skip", "true", "false",
                                     "not", "and", "or", "begin",
                                     "end"]
           , Token.reservedOpNames = ["+", "-", "*", "/", ":=",
                                     "<", ">", "and", "or", "not"]
           }

lexer = Token.makeTokenParser languageDef

-- Bind lexical parser locally

identifier = Token.identifier lexer
reserved   = Token.reserved   lexer
reservedOp = Token.reservedOp lexer
parens     = Token.parens     lexer
integer    = Token.integer    lexer
semi       = Token.semi       lexer
whiteSpace = Token.whiteSpace lexer
brackets   = Token.brackets   lexer
squares    = Token.squares    lexer

-- 
-- Parsers
--

mainParser :: Parser Stmt
mainParser = whiteSpace >> statement

statement :: Parser Stmt
statement = parens statement
        <|> seqOfStmt

seqOfStmt =
  do list <- (sepBy1 statement' semi)
     return (if length list == 1 then head list else Seq list)


statement' :: Parser Stmt
statement' = beginStmt
         <|> ifStmt
         <|> whileStmt
         <|> skipStmt
         <|> assignStmt
         <?> "unrecognized statement"

-- Statement parsers

beginStmt :: Parser Stmt
beginStmt = 
  do reserved "begin"
     d <- decl
     s <- statement
     return Begin d s

decl :: Parser [Decl]
decl = 
  do squares
     d <- identifier
     char ','
     return d

ifStmt :: Parser Stmt
ifStmt = 
    do reserved "if"
       cond   <- bExpression
       reserved "then"
       stmt1  <- statement
       reserved "else"
       stmt2  <- statement
       return $ IfThenElse cond stmt1 stmt2

whileStmt :: Parser Stmt
whileStmt = 
    do reserved "while"
       cond   <- bExpression
       reserved "do"
       stmt   <- statement
       return $ While cond stmt

assignStmt :: Parser Stmt
assignStmt = 
    do var  <- identifier
       reservedOp ":="
       expr <- aExpression
       return $ Assign var expr

skipStmt :: Parser Stmt
skipStmt = reserved "skip" >> return Skip

aExpression :: Parser IntExp
aExpression = buildExpressionParser aOperators aTerm

bExpression :: Parser BoolExp
bExpression = buildExpressionParser bOperators bTerm

aOperators = [ [Prefix (reservedOp "-"   >>   return (Neg               ))          ]
             , [Infix  (reservedOp "*"   >>   return (IntBinary Multiply)) AssocLeft] 
             , [Infix  (reservedOp "/"   >>   return (IntBinary Divide  )) AssocLeft]
             , [Infix  (reservedOp "+"   >>   return (IntBinary Add     )) AssocLeft]
             , [Infix  (reservedOp "-"   >>   return (IntBinary Subtract)) AssocLeft]
             ]

bOperators = [ [Prefix (reserved "not"   >>   return (Not                ))          ]
             , [Infix  (reserved "and"   >>   return (BoolBinary And     )) AssocLeft]
             , [Infix  (reserved "or"    >>   return (BoolBinary Or      )) AssocLeft]
             ]

aTerm = parens aExpression
    <|> liftM IVar identifier
    <|> liftM ICon integer

bTerm = parens bExpression
    <|> (reserved "true" >> return (BoolConst True))
    <|> (reserved "false">> return (BoolConst False))
    <|> rExpression

rExpression =
    do a1 <- aExpression
       op <- relation
       a2 <- aExpression
       return $ RelBinary op a1 a2

relation = (reservedOp ">" >> return Greater)
       <|> (reservedOp "<" >> return Less)
       <|> (reservedOp "=" >> return Equals)


parseString :: String -> Stmt
parseString str = case parse mainParser "" str of
                    Left e  -> error $ show e
                    Right r -> r

parseFile file = 
  do program <- readFile file
     case parse mainParser "" program of
       Left e -> print e >> fail "parse error"
       Right r -> return r



