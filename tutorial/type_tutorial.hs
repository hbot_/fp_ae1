
type Var = String

data Number = NumInt Integer
            | NumDbl Double
            deriving (Read, Show, Eq, Ord)

class Numeric a where
  (.+.) :: a -> a -> a
  (.-.) :: a -> a -> a
  (.*.) :: a -> a -> a
  (./.) :: a -> a -> a
  (.^.) :: a -> a -> a
  fromDbl :: a -> Double
  fromInt :: a -> Integer

instance Numeric Number where
  (.+.) (NumInt a) (NumInt b) = NumInt (a + b)
  (.+.) (NumDbl a) (NumDbl b) = NumDbl (a + b)
  (.+.) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) + b)
  (.+.) (NumDbl a) (NumInt b) = NumDbl (a + (fromInteger b))

  (.-.) (NumInt a) (NumInt b) = NumInt (a - b)
  (.-.) (NumDbl a) (NumDbl b) = NumDbl (a - b)
  (.-.) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) - b)
  (.-.) (NumDbl a) (NumInt b) = NumDbl (a - (fromInteger b))

  (.*.) (NumInt a) (NumInt b) = NumInt (a * b)
  (.*.) (NumDbl a) (NumDbl b) = NumDbl (a * b)
  (.*.) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) * b) 
  (.*.) (NumDbl a) (NumInt b) = NumDbl (a * (fromInteger b))

  (./.) (NumInt a) (NumInt b) = NumInt (a `div` b)
  (./.) (NumDbl a) (NumDbl b) = NumDbl (a / b)
  (./.) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) / b) 
  (./.) (NumDbl a) (NumInt b) = NumDbl (a / (fromInteger b))

  (.^.) (NumInt a) (NumInt b) = NumInt (a ^ b)
  (.^.) (NumDbl a) (NumDbl b) = NumDbl (a ** b)
  (.^.) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) ** b) 
  (.^.) (NumDbl a) (NumInt b) = NumDbl (a ** (fromInteger b))

  fromDbl (NumDbl a) = a
  fromInt (NumInt a) = a

{-
instance Num Number where
  (+) (NumInt a) (NumInt b) = NumInt (a + b)
  (+) (NumDbl a) (NumDbl b) = NumDbl (a + b)
  (+) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) + b)
  (+) (NumDbl a) (NumInt b) = NumDbl (a + (fromInteger b))

  (-) (NumInt a) (NumInt b) = NumInt (a - b)
  (-) (NumDbl a) (NumDbl b) = NumDbl (a - b)
  (-) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) - b)
  (-) (NumDbl a) (NumInt b) = NumDbl (a - (fromInteger b))

  (*) (NumInt a) (NumInt b) = NumInt (a * b)
  (*) (NumDbl a) (NumDbl b) = NumDbl (a * b)
  (*) (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) * b) 
  (*) (NumDbl a) (NumInt b) = NumDbl (a * (fromInteger b))

  abs a = if a > 0 then a else ((-1) * a)
  signum a = if a < 0 then -1 else 1
  fromInteger a = NumInt a

ndiv :: Number -> Number -> Number
ndiv (NumInt a) (NumInt b) = NumInt (a `div` b)
ndiv (NumDbl a) (NumDbl b) = NumDbl (a / b)
ndiv (NumInt a) (NumDbl b) = NumDbl ((fromInteger a) / b) 
ndiv (NumDbl a) (NumInt b) = NumDbl (a / (fromInteger b))
-}
data AExpre = IVar Var
            | ICon Number

