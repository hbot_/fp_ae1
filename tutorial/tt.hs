
data Number = Integer
            | Double  
            deriving (Show, Read)

--fun :: Number -> Number -> Either Integer Double
--fun a b = 0
--
readMaybe :: (Read a) => String -> Maybe a
readMaybe s = case reads s of
                [(x, "")] -> Just x
                _         -> Nothing
