import Data.Maybe
import Data.Map 

type Var = String
type Env = [[(Var, Int)]]



{-
mlookup "a" [[("x",1), ("y", 2)], [("x",5)], [("a", 121)]]
-}
mlookup :: Var -> Env -> Maybe Int
mlookup x []   = error $ "No variable declaration found: " ++ x
mlookup v e    = if Prelude.lookup v (head e) == Nothing then mlookup v (tail e)
                 else Prelude.lookup v (head e)

{-
updateScope "a" 1 [("a", 121)]
-}

-- this expression can be used to delete from a list
-- by making f _ = Nothing
--
-- if the key value isn't present in the list, it adds it
-- with the specified value

updateScope key val list = toList $ alter f key (fromList list)
                           where 
                              f _ = Just val

initial = []

