import System.IO
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

-- Binary boolean expressions

data BExpr = BoolConst Bool
           | Not BExpr
           | BBinary BBinOp BExpr BExpr
           | RBinary RBinOp AExpr AExpr
           deriving (Show)

-- Binary boolean operators

data BBinOp = And | Or deriving (Show)

data RBinOp = Greater | Less deriving (Show)

-- Arithmetic Expressions

data AExpr = Var String
           | IntConst Integer
           | Neg AExpr
           | ABinary ABinOp AExpr AExpr
           deriving (Show)

data ABinOp = Add
            | Subtract
            | Multiply
            | Divide
            deriving (Show)

-- Statements

type Decl = AExpr

data Stmt = Begin [AExpr] [Stmt] 
          | Seq [Stmt]
          | Assign String AExpr
          | If BExpr Stmt Stmt
          | While BExpr Stmt
          | Skip
          | End
          deriving (Show)

languageDef = 
    emptyDef { Token.commentStart    = "/*"
             , Token.commentEnd      = "*/"
             , Token.commentLine     = "//"
             , Token.identStart      = letter
             , Token.identLetter     = alphaNum
             , Token.reservedNames   = ["if", "then", "else", "while",
                                     "do", "skip", "true", "false",
                                     "not", "and", "or", "begin", "end"]
             , Token.reservedOpNames = ["+", "-", "*", "/", ":=",
                                        "<", ">", "and", "or", "not"]
             }

lexer = Token.makeTokenParser languageDef

-- Binding lexical parsers

identifier = Token.identifier lexer
reserved   = Token.reserved   lexer
reservedOp = Token.reservedOp lexer
parens     = Token.parens     lexer
integer    = Token.integer    lexer
semi       = Token.semi       lexer
whiteSpace = Token.whiteSpace lexer
brackets   = Token.brackets   lexer
commaSep   = Token.commaSep   lexer
braces     = Token.braces     lexer

whileParser :: Parser Stmt
whileParser = whiteSpace >> statement -- sequencing operator


statement :: Parser Stmt
statement =   parens statement
          <|> sequenceOfStmt

sequenceOfStmt = 
  do list <- (sepBy1 statement' semi)
     return $ if length list == 1 then head list else Seq list

statement' :: Parser Stmt
statement' = beginStmt
         <|> ifStmt
         <|> whileStmt
         <|> skipStmt
         <|> assignStmt


-- Parsing each different statement

beginStmt :: Parser Stmt
beginStmt =
    do reserved "begin" 
       d <- decl
       s <- seqOfStmt 
       reserved "end"
       return $ Begin d s

seqOfStmt = 
  do list <- (sepBy1 statement' semi)
     return list

decl :: Parser [AExpr]
decl =  
    do list <- brackets (commaSep aExpression)
       return list
       
ifStmt :: Parser Stmt
ifStmt = 
    do reserved "if"
       cond   <- bExpression
       reserved "then"
       stmt1  <- statement
       reserved "else"
       stmt2  <- statement
       return $ If cond stmt1 stmt2

whileStmt :: Parser Stmt
whileStmt = 
    do reserved "while"
       cond   <- bExpression
       reserved "do"
       stmt   <- statement
       return $ While cond stmt

assignStmt :: Parser Stmt
assignStmt = 
    do var  <- identifier
       reservedOp ":="
       expr <- aExpression
       return $ Assign var expr

skipStmt :: Parser Stmt
skipStmt = reserved "skip" >> return Skip

aExpression :: Parser AExpr
aExpression = buildExpressionParser aOperators aTerm

bExpression :: Parser BExpr
bExpression = buildExpressionParser bOperators bTerm

aOperators = [ [Prefix (reservedOp "-"   >>   return (Neg             ))          ]
             , [Infix  (reservedOp "*"   >>   return (ABinary Multiply)) AssocLeft] 
             , [Infix  (reservedOp "/"   >>   return (ABinary Divide  )) AssocLeft]
             , [Infix  (reservedOp "+"   >>   return (ABinary Add     )) AssocLeft]
             , [Infix  (reservedOp "-"   >>   return (ABinary Subtract)) AssocLeft]
             ]

bOperators = [ [Prefix (reserved "not"   >>   return (Not             ))          ]
             , [Infix  (reserved "and"   >>   return (BBinary And     )) AssocLeft]
             , [Infix  (reserved "or"    >>   return (BBinary Or      )) AssocLeft]
             ]

aTerm = parens aExpression
    <|> liftM Var identifier
    <|> liftM IntConst integer

bTerm = parens bExpression
    <|> (reserved "true" >> return (BoolConst True))
    <|> (reserved "false">> return (BoolConst False))
    <|> rExpression

rExpression =
    do a1 <- aExpression
       op <- relation
       a2 <- aExpression
       return $ RBinary op a1 a2

relation = (reservedOp ">" >> return Greater)
       <|> (reservedOp "<" >> return Less)


parseString :: String -> Stmt
parseString str = case parse whileParser "" str of
                    Left e  -> error $ show e
                    Right r -> r

parseFile file = 
  do program <- readFile file
     case parse whileParser "" program of
       Left e -> print e >> fail "parse error"
       Right r -> return r

